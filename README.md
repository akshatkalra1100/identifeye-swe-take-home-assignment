# Identifeye SWE Take Home Assignment  


## Name 
This is the take home coding assignment for Identifeye Health's Summer 2023 SWE Internship. 

## File Anatomy
- identifeye.txt : Contains the baseline test cases to evaluate the working of the code. 
- Identifeye.py : The driver file to process all the lines read by the lineread.py file and evaluate the output specifications. 
- lineread.py : File to read all the commands from a text file and store that in a list of commands, to be further used by the processing file. 

## Structure of Test Cases 
- ADD PATIENT 123 JOHN DOE : For adding a new patient with their specified id. 
- ADD EXAM 123 456 : For adding a new exam with the respective exam_id and patient_id. 
- DEL PATIENT 123 : For deleting a patient through the supplied patient id. 
- **DEL EXAM 123 456** : For deleting a specific exam record for a particular patient. 

