"""
Assumption : For the command of deleting an exam record, I have assumed that the text file must also supply the patient id for whom that particular
exam has to be deleted for simply a couple of reasons: 

a) Intuitively, it makes sense that the system should the know the patient id for whom the exam is being deleted, since seldom does any situation 
occur when we have to delete a particular test conducted for all the patients regardless. 

b) Even if such situation occurs, then we are assuming that this exam id will be common to multiple patients, and the problem statement did not 
supply any information for that being possible, which is why I thought it's safe to make the assumption I did and better rely on object-oriented 
methods of solving this problem. 

VARIABLE NAMING CONVENTION : 
pid : patient id 
eid : exam id 
p_id : patient id (in the scope of class patient)

"""

import lineread
# print(lineread.list_of_commands)

class patient :

    def __init__(self, p_id, name) :
        self.name = name
        self.p_id = p_id
        self.examcount = 0
        self.exams = {}
        
    def add_exam(self, exam_id) :
        self.examcount += 1
        self.exams[exam_id] = exam(exam_id)

    def del_exam(self, exam_id) :
        
        if exam_id in self.exams :
            del self.exams[exam_id]
            self.examcount -= 1
        
    def output(self) :
        print("Name:", self.name, ", Id:", self.p_id, ", Exam Count:", self.examcount)
    

class exam :

    def __init__(self, exam_id) :
        self.exam_id = exam_id
        # self.p_id = p_id

#Function to add a new patient record 
def add_patient(id, name) :

    global patient_directory

    new_patient = patient(id, name)
    patient_directory[id] = new_patient

#Function to add a new exam record 
def add_exam(pid, eid) :
  
    global patient_directory

    new_exam = exam(eid)
    patient_directory[pid].add_exam(eid)

#Function to delete a patient record 
def delete_patient(pid, name = None) :

    global patient_directory

    #Deleting the object as the value of the key 
    del patient_directory[pid]


#Function to delete an exam record 
def delete_exam(pid, eid) :
   
    global patient_directory

    patient_directory[pid].del_exam(eid)


#Script begins 

patient_directory = {}
exam_directory = {}

#Implementing a concept akin to Switch-case in python, via Dictionaries beholding function objects as their value to serve as a menu-driven or transitory state program. 
processing_cases = \
{
    'ADD PATIENT' : add_patient,
    'ADD EXAM' : add_exam,
    'DEL PATIENT' : delete_patient,
    'DEL EXAM' : delete_exam
}

#Adding the code to load every command one by one 

for command in lineread.list_of_commands :
    
    # Split the command into a list of words
    words = command.split()

    # Join the first two words into a new string
    case = " ".join(words[:2])

    if len(words) > 3 :
        fourth = " ".join(words[3:])

    if len(words) == 3 :
        third = words[2]
        processing_cases[case](third)

    else :
        third = words[2]
        processing_cases[case](third, fourth)

for patient in patient_directory.keys() :
    patient_directory[patient].output()
